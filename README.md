This will eventually be a set of language plugins to help with sysadmin work, for example
to get full code completion on configuration files for server software.

Currently it only has a parser for the [TOML](https://github.com/toml-lang/toml/) language.

# Dependencies
- Qt5
- [re2c](http://re2c.org/) 0.13.7 or later
