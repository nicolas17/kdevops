// Experimental KDevelop language plugin
// Copyright (c) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef KDEVPLUGIN_TEXTRANGE_H
#define KDEVPLUGIN_TEXTRANGE_H

/** \file
 * Classes representing text cursors and ranges.
 *
 * Essentially this is the same as what %KTextEditor has in its Cursor and Range classes,
 * but I didn't want to make the parser depend on %KTextEditor.
 */

#include <QtGlobal>

/**
 * A line/column pair representing a position in a text file.
 *
 * Lines and columns start at 0. If both are -1, it represents an invalid position.
 */
struct TextCursor {
    int line, column;
    /// Initialize an invalid TextCursor. isValid will return @c false.
    TextCursor()
        : line(-1)
        , column(-1)
    {}
    /// Initialize a TextCursor with the given \a line and \a column.
    TextCursor(int line, int column)
        : line(line)
        , column(column)
    {
    }
    /// Returns whether this cursor is valid.
    /// An invalid cursor has both line and column set to -1,
    /// and it's created by the default constructor.
    bool isValid() const {
        return line != -1 && column != -1;
    }
};

/**
 * Compares if two TextCursor%s are equal.
 *
 * Simply defined as having the same line and column.
 */
inline bool operator==(const TextCursor& lhs, const TextCursor& rhs) {
    return lhs.line == rhs.line && lhs.column == rhs.column;
}
/**
 * A pair of start and end TextCursor%s representing a text range in a file.
 *
 * Lines and columns start at 0.
 */
struct TextRange {
    TextCursor start;
    TextCursor end;

    /// Initialize an invalid TextRange. isValid will return @c false.
    TextRange() {}
    /// Initialize a TextCursor with the given start and end lines and columns.
    /// The starting cursor \e must be before the ending one.
    TextRange(int startLine, int startCol, int endLine, int endCol):
        start(startLine, startCol),
        end(endLine, endCol)
    {
        Q_ASSERT(startLine <= endLine);
        Q_ASSERT(startCol <= endCol);
    }
    /// Returns whether this range is valid.
    /// An invalid range has invalid start and end cursors
    /// and it's created by the default constructor.
    bool isValid() const {
        return start.isValid() && end.isValid();
    }
};

/**
 * Compares if two TextRanges%s are equal.
 *
 * Simply defined as having equal line and column for the start and end cursors.
 */
inline bool operator==(const TextRange& lhs, const TextRange& rhs) {
    return lhs.start == rhs.start && lhs.end == rhs.end;
}
/**
 * Returns a C string with a text representation of the given \a cursor.
 *
 * The string is allocated with qstrdup, and it must be freed with delete[] by the caller.
 * This function is automatically used by the QtTest framework if a QCOMPARE check fails,
 * to display the two differing objects on the console.
 */
inline char* toString(const TextCursor& cursor) {
    return qstrdup(QString::fromLatin1("TextCursor <%1,%2>")
        .arg(cursor.line).arg(cursor.column)
        .toLatin1().constData());
}
/**
 * Returns a C string with a text representation of the given \a range.
 *
 * The string is allocated with qstrdup, and it must be freed with delete[] by the caller.
 * This function is automatically used by the QtTest framework if a QCOMPARE check fails,
 * to display the two differing objects on the console.
 */
inline char* toString(const TextRange& range) {
    return qstrdup(QString::fromLatin1("TextRange <%1,%2> -> <%3,%4>")
        .arg(range.start.line).arg(range.start.column)
        .arg(range.end.line).arg(range.end.column)
        .toLatin1().constData());
}

#endif
