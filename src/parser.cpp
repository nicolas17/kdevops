/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.h"
#include "lexer.h"
#include "ast.h"

Parser::Parser(Lexer* lexer)
: m_lexer(lexer)
{
}

/// Skips to the first token in the next line (ie. the token following the next EOL token)
/// or to the end of the file if there are no more lines.
Token Parser::skipToEOL() {
    Token tok = m_lexer->currentTok();
    while (tok.type() != TokenType::END_OF_LINE && tok.type() != TokenType::END_OF_FILE) {
        tok = m_lexer->next();
    }
    return m_lexer->next();
}

std::unique_ptr<DocumentNode> Parser::parse()
{
    auto doc = std::make_unique<DocumentNode>();

    // get first token
    m_lexer->next();

    TableNode* currentTable = nullptr;

    while (m_lexer->currentTok().type() != TokenType::END_OF_FILE) {
        const Token& tok = m_lexer->currentTok();
        if (tok.type() == TokenType::IDENT) {
            auto keyVal = parseKeyVal();
            if (keyVal) {
                if (currentTable) {
                    currentTable->keyvals.push_back(std::move(keyVal));
                } else {
                    doc->keyvals.push_back(std::move(keyVal));
                }
            }
        } else if (tok.type() == TokenType::LBRACKET) {
            auto table = parseTable();
            if (table) {
                currentTable = table.get();
                doc->tables.push_back(std::move(table));
            }
            continue;
        } else if (tok.type() == TokenType::END_OF_LINE) {
            m_lexer->next();
            continue;
        } else {
            skipToEOL();
            continue;
        }
    }

    return doc;
}

std::unique_ptr<KeyValNode> Parser::parseKeyVal()
{
    Token tok = m_lexer->currentTok();
    Q_ASSERT(tok.type() == TokenType::IDENT);

    auto node = std::make_unique<KeyValNode>();
    node->key = KeyNode{tok.strValue(), tok.range};

    // check for equals sign
    tok = m_lexer->next();
    if (tok.type() != TokenType::EQUAL) {
        skipToEOL();
        return node;
    }

    // get value
    tok = m_lexer->next(Lexer::ValueLexerMode);
    std::unique_ptr<ValueNode> value;
    if (tok.type() == TokenType::STRING) {
        value = std::make_unique<StringValueNode>(tok.strValue(), tok.range);
    } else if (tok.type() == TokenType::BOOL) {
        value = std::make_unique<BoolValueNode>(tok.intValue() == 1, tok.range);
    } else if (tok.type() == TokenType::INT) {
        value = std::make_unique<IntValueNode>(tok.intValue(), tok.range);
    } else {
        skipToEOL();
        return node;
    }

    //next token should be EOL (or EOF)
    tok = m_lexer->next();
    if (tok.type() != TokenType::END_OF_LINE && tok.type() != TokenType::END_OF_FILE) {
        // if it's not EOL, skip until we find it, and return what we have
        skipToEOL();
        return node;
    }
    m_lexer->next();

    node->value = std::move(value);
    return node;
}
std::unique_ptr<TableNode> Parser::parseTable()
{
    Token tok = m_lexer->currentTok();
    Q_ASSERT(tok.type() == TokenType::LBRACKET);

    // get name
    tok = m_lexer->next();
    if (tok.type() != TokenType::IDENT) {
        skipToEOL();
        return nullptr;
    }
    QString tableName{tok.strValue()};
    TextRange nameRange{tok.range};

    // check for closing bracket
    tok = m_lexer->next();
    if (tok.type() != TokenType::RBRACKET) {
        skipToEOL();
        return nullptr;
    }

    auto table = std::make_unique<TableNode>();
    table->name = std::move(tableName);
    table->nameRange = std::move(nameRange);

    //next token should be EOL (or EOF)
    tok = m_lexer->next();
    if (tok.type() != TokenType::END_OF_LINE && tok.type() != TokenType::END_OF_FILE) {
        // if it's not EOL, skip until we find it, and return what we have
        skipToEOL();
        return table;
    }
    m_lexer->next();

    return table;
}
