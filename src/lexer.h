/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOML_LEXER_H
#define TOML_LEXER_H

#include <QString>

#include "textrange.h"

enum class TokenType: unsigned char {
    UNKNOWN,
    END_OF_FILE,
    END_OF_LINE,
    IDENT,
    STRING,
    LBRACKET,
    RBRACKET,
    DOUBLE_LBRACKET,
    DOUBLE_RBRACKET,
    EQUAL,
    DOT,
    INT,
    BOOL
};

class Token {
public:
    inline Token(){}
    inline Token(TokenType type);
    inline Token(TokenType type, const QString& strValue);

    TokenType type() const { return m_type; }
    QString strValue() const { return m_strValue; }
    int intValue() const { return m_intValue; }

    TokenType m_type;
    QString m_strValue;
    int m_intValue;
    TextRange range;
};
Token::Token(TokenType type)
: m_type(type)
{
}
Token::Token(TokenType type, const QString& strValue)
: m_type(type)
, m_strValue(strValue)
{
}


class Lexer {
public:
    enum LexerMode {
        MainLexerMode, ValueLexerMode
    };

    Lexer(const QString& code);
    Token currentTok();
    Token next(LexerMode mode = MainLexerMode);

private:
    const QString m_code;
    const QChar* const m_codeBegin;
    const QChar* const m_codeEnd;
    const QChar* m_cursor;
    const QChar* m_lineStart;
    Token m_token;
    int m_line;

    int column() const;
    TextCursor textCursor() const;
};

#endif
