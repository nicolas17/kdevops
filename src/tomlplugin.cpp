/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tomlplugin.h"

#include <QDebug>
#include <KPluginFactory>

#include <serialization/indexedstring.h>
#include <language/highlighting/codehighlighting.h>

#include "tomlparsejob.h"

K_PLUGIN_FACTORY_WITH_JSON(KDevTOMLPluginFactory, "tomlplugin.json", registerPlugin<KDevTOMLPlugin>(); )

KDevTOMLPlugin::KDevTOMLPlugin(QObject *parent, const QVariantList& args) :
    KDevelop::IPlugin(QStringLiteral("tomlplugin"), parent),
    m_highlighter(new KDevelop::CodeHighlighting(this))
{
    qDebug() << "My plugin is loaded!";
}

QString KDevTOMLPlugin::name() const
{
    static const QString langName = QStringLiteral("lalalala");
    return langName;
}
KDevelop::ParseJob* KDevTOMLPlugin::createParseJob(const KDevelop::IndexedString& url)
{
    qDebug() << "asked to create a parse job for" << url;
    return new KDevTOMLParseJob(url, this);
}
KDevelop::ICodeHighlighting * KDevTOMLPlugin::codeHighlighting() const
{
    return m_highlighter;
}

#include "tomlplugin.moc"
