/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOML_AST_H
#define TOML_AST_H

#include <vector>
#include <memory>

#include <QString>

#include "ast_visitor.h"
#include "textrange.h"

class AstNode {
public:
    virtual ~AstNode(){}
    virtual void accept(AstVisitor* visitor) = 0;
};

class KeyNode: public AstNode {
public:
    QString name;
    TextRange range;

    KeyNode() = default;
    KeyNode(const QString& name, const TextRange& range):
        name(name),
        range(range)
    {}

    void accept(AstVisitor* visitor) override {
        visitor->visit(this);
    }
};
class ValueNode: public AstNode {
public:
    TextRange range;

    ValueNode(const TextRange& range):
        range(range)
    {}

    // intentionally no accept(), so it remains abstract
};
class StringValueNode: public ValueNode {
public:
    QString strValue;

    StringValueNode() = default;
    StringValueNode(const QString& strValue, const TextRange& range):
        ValueNode(range),
        strValue(strValue)
    {}

    void accept(AstVisitor* visitor) override {
        visitor->visit(this);
    }
};
class BoolValueNode: public ValueNode {
public:
    bool boolValue;

    BoolValueNode() = default;
    BoolValueNode(bool boolValue, const TextRange& range):
        ValueNode(range),
        boolValue(boolValue)
    {}

    void accept(AstVisitor* visitor) override {
        visitor->visit(this);
    }
};
class IntValueNode: public ValueNode {
public:
    int intValue;

    IntValueNode() = default;
    IntValueNode(int intValue, const TextRange& range):
        ValueNode(range),
        intValue(intValue)
    {}

    void accept(AstVisitor* visitor) override {
        visitor->visit(this);
    }
};

class KeyValNode: public AstNode {
public:
    KeyNode key;
    std::unique_ptr<ValueNode> value;

    void accept(AstVisitor* visitor) override {
        if (visitor->visit(this)) {
            key.accept(visitor);
            value->accept(visitor);
        }
    }
};

class TableNode: public AstNode {
public:
    QString name;
    TextRange nameRange;

    std::vector<std::unique_ptr<KeyValNode>> keyvals;

    void accept(AstVisitor* visitor) override {
        if (visitor->visit(this)) {
            for (auto& keyval: keyvals) {
                keyval->accept(visitor);
            }
        }
    }
};

class DocumentNode: public AstNode {
public:
    std::vector<std::unique_ptr<KeyValNode>> keyvals;
    std::vector<std::unique_ptr<TableNode>> tables;

    void accept(AstVisitor* visitor) override {
        if (visitor->visit(this)) {
            for (auto& keyval: keyvals) {
                keyval->accept(visitor);
            }
            for (auto& table: tables) {
                table->accept(visitor);
            }
        }
    }
};

#endif
