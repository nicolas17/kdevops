/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOML_PARSER_H
#define TOML_PARSER_H

#include <memory>

class Lexer;
class DocumentNode;
class KeyValNode;
class TableNode;
class Token;

class Parser {
public:
    explicit Parser(Lexer* lexer);

    std::unique_ptr<DocumentNode> parse();
private:
    Lexer* m_lexer;

    std::unique_ptr<KeyValNode> parseKeyVal();
    std::unique_ptr<TableNode> parseTable();
    Token skipToEOL();
};

#endif
