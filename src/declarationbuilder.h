/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOML_DECLARATIONBUILDER_H
#define TOML_DECLARATIONBUILDER_H

#include <language/duchain/builders/abstractdeclarationbuilder.h>

#include "contextbuilder.h"

class TOMLDeclarationBuilder: public KDevelop::AbstractDeclarationBuilder<AstNode, AstNode, TOMLContextBuilder>
{
    bool visit(KeyValNode* node) override;
    bool visit(TableNode* table) override;
};

#endif
