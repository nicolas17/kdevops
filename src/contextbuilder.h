/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOML_CONTEXTBUILDER_H
#define TOML_CONTEXTBUILDER_H

#include <language/duchain/builders/abstractcontextbuilder.h>

#include "ast_visitor.h"

class TOMLContextBuilder: public KDevelop::AbstractContextBuilder<AstNode, AstNode>, public AstVisitor
{
    KDevelop::TopDUContext* newTopContext(const KDevelop::RangeInRevision& range, KDevelop::ParsingEnvironmentFile* file) override;

    void startVisiting(AstNode* node) override;
    void setContextOnNode(AstNode* node, KDevelop::DUContext* context) override;
    KDevelop::DUContext* contextFromNode(AstNode* node) override;
    KDevelop::RangeInRevision editorFindRange(AstNode* fromNode, AstNode* toNode) override;
    KDevelop::QualifiedIdentifier identifierForNode(AstNode* node) override;
};

#endif
