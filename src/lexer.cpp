/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <QString>
#include <QDebug>

#include "lexer.h"

// this defines YYMAXFILL
/*!max:re2c*/

/// Add YYMAXFILL null characters at the end of the string
QString addNullPadding(const QString& code) {
    QString result(code);
    result.reserve(code.size() + YYMAXFILL);
    for (int i=0; i<YYMAXFILL; ++i) {
        result.append(QLatin1Char('\0'));
    }
    Q_ASSERT(result.size() == code.size() + YYMAXFILL);
    return result;
}

Lexer::Lexer(const QString& code)
: m_code(addNullPadding(code))
, m_codeBegin(m_code.unicode())
, m_codeEnd(m_codeBegin + m_code.size()) // this includes the padding
, m_cursor(m_codeBegin)
, m_lineStart(m_codeBegin)
, m_line(0)
{
}

int Lexer::column() const
{
    return m_cursor - m_lineStart;
}

TextCursor Lexer::textCursor() const
{
    return {m_line, column()};
}

Token Lexer::currentTok()
{
    return m_token;
}

static int lexNumber(const QChar* begin, const QChar* end) {
    // TODO this function will lex multiple consecutive underscores,
    // which is invalid, it should give at least a warning
    int n = 0;
    bool neg = false;

    if (*begin == '-') {
        neg = true;
        ++begin;
    } else if (*begin == '+') {
        //skip over plus sign without doing anything
        ++begin;
    }

    while (begin != end) {
        char ch = begin->toLatin1();
        if ('0' <= ch && ch <= '9') {
            n = (n*10) + (ch - '0');
        } else if (ch == '_') {
            // ignore
        } else {
            // TODO give error somehow
        }
        ++begin;
    }
    if (neg) {
        n *= -1;
    }
    return n;
}

/*!types:re2c*/

Token Lexer::next(LexerMode mode)
{
    // do nothing if we're already at EOF
    if (currentTok().type() == TokenType::END_OF_FILE) {
        return m_token;
    }

    const QChar* tok;
    const QChar* YYMARKER;
    TextCursor tokPos;

#define YYFILL(n) { qFatal("Called YYFILL, but we should have hit a NUL already"); }
#define YYGETCONDITION() (mode == ValueLexerMode ? yycvalue : yycmain)

    // the loop lets us start over if we find just whitespace
    for (;;) {
        tok = m_cursor;
        tokPos = textCursor();

        /*!re2c
        re2c:flags:x = 1;
        re2c:define:YYCTYPE = QChar;
        re2c:define:YYCURSOR = m_cursor;
        re2c:define:YYLIMIT = m_codeEnd;

        <*> [ \t] { continue; }
        <*> * { std::cout << "Unrecognized character '" << tok->toLatin1() << "'\n"; continue; }
        <*> "#" .* { continue; }
        <*> "\n" {
            m_line++;
            m_lineStart = m_cursor;
            m_token = Token(TokenType::END_OF_LINE);
            break;
        }
        <value> "true" {
            m_token = Token(TokenType::BOOL);
            m_token.m_intValue = 1;
            break;
        }
        <value> "false" {
            m_token = Token(TokenType::BOOL);
            m_token.m_intValue = 0;
            break;
        }
        <value> [-+]?[0-9_]+ {
            m_token = Token(TokenType::INT);
            int n = lexNumber(tok, m_cursor);
            m_token.m_intValue = n;
            break;
        }
        <main> [A-Za-z_-][A-Za-z0-9_-]* {
            m_token = Token(TokenType::IDENT, QString(tok, m_cursor-tok));
            break;
        }
        <main> "=" {
            m_token = Token(TokenType::EQUAL);
            break;
        }
        <*> "[" {
            m_token = Token(TokenType::LBRACKET);
            break;
        }
        <*> "]" {
            m_token = Token(TokenType::RBRACKET);
            break;
        }
        <main> "[[" {
            m_token = Token(TokenType::DOUBLE_LBRACKET);
            break;
        }
        <main> "]]" {
            m_token = Token(TokenType::DOUBLE_RBRACKET);
            break;
        }
        <*> '"' [^"\x00]* '"' {
            m_token = Token(TokenType::STRING, QString(tok+1, m_cursor-tok-2));
            break;
        }
        <*> "\x00" {
            m_token = Token(TokenType::END_OF_FILE);
            break;
        }
        */
    }
    m_token.range.start = tokPos;
    m_token.range.end = textCursor();
    return m_token;
}
