#include <../../kdevplatform-prefix/include/kdevplatform/debugger/breakpoint/breakpoint.h>
/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOML_TOMLPARSEJOB_H
#define TOML_TOMLPARSEJOB_H

#include <language/backgroundparser/parsejob.h>

class KDevTOMLParseJob: public KDevelop::ParseJob
{
public:
    KDevTOMLParseJob(const KDevelop::IndexedString& url, KDevelop::ILanguageSupport* langSupport);
    void run(ThreadWeaver::JobPointer self, ThreadWeaver::Thread* thread) override;
};

#endif
