/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOML_AST_VISITOR_H
#define TOML_AST_VISITOR_H

class AstNode;
class KeyValNode;
class KeyNode;
class StringValueNode;
class BoolValueNode;
class IntValueNode;
class TableNode;
class DocumentNode;

class AstVisitor {
public:
    ~AstVisitor() {}

    // The return value specifies whether the visitor should automatically recurse
    // into sub-nodes. Return false if you're doing your own recursion, or if you
    // don't care about child nodes.
    virtual bool visit(KeyValNode*) { return true; }
    virtual bool visit(KeyNode*) { return true; }
    virtual bool visit(StringValueNode*) { return true; }
    virtual bool visit(BoolValueNode*) { return true; }
    virtual bool visit(IntValueNode*) { return true; }
    virtual bool visit(TableNode*) { return true; }
    virtual bool visit(DocumentNode*) { return true; }
};

#endif
