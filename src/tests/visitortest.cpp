/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QtTest>

#include "lexer.h"
#include "parser.h"
#include "ast.h"

class VisitorTest : public QObject
{
    Q_OBJECT
private slots:

    void testKV();
    void testSelectiveRecursion();
    void testSelectiveRecursion_data();
};

QTEST_APPLESS_MAIN(VisitorTest);

class DebugVisitor: public AstVisitor {
public:
    QStringList result;

    bool visit(KeyNode* node) override {
        result.append(QStringLiteral("key:%1").arg(node->name));
        return true;
    }
    bool visit(StringValueNode* node) override {
        result.append(QStringLiteral("value:%1").arg(node->strValue));
        return true;
    }
    bool visit(KeyValNode* node) override {
        result.append(QStringLiteral("begin:keyval"));
        node->key.accept(this);
        node->value->accept(this);
        result.append(QStringLiteral("end:keyval"));
        return false; // we do our own recursion
    }
    bool visit(DocumentNode* node) override {
        result.append(QStringLiteral("begin:document"));
        for (auto& keyval: node->keyvals) {
            keyval->accept(this);
        }
        result.append(QStringLiteral("end:document"));
        return false; // we do our own recursion
    }
};
class RecursingVisitor: public AstVisitor {
public:
    QStringList result;
    bool recurseIntoDoc = false;
    bool recurseIntoKeyVal = false;

    bool visit(KeyNode* node) override {
        result.append(QStringLiteral("key"));
        return true;
    }
    bool visit(StringValueNode* node) override {
        result.append(QStringLiteral("value"));
        return true;
    }
    bool visit(KeyValNode* node) override {
        result.append(QStringLiteral("keyval"));
        return recurseIntoKeyVal;
    }
    bool visit(DocumentNode* node) override {
        result.append(QStringLiteral("doc"));
        return recurseIntoDoc;
    }
};

DocumentNode createTestDoc() {
    auto keyval = std::make_unique<KeyValNode>();
    keyval->key = KeyNode{QStringLiteral("one"), TextRange(0,0,0,3)};
    keyval->value = std::make_unique<StringValueNode>(QStringLiteral("two"), TextRange(0,6,0,11));

    DocumentNode doc;
    doc.keyvals.push_back(std::move(keyval));
    return doc;
}

void VisitorTest::testKV()
{
    DocumentNode doc = createTestDoc();

    DebugVisitor visitor;
    doc.accept(&visitor);

    QCOMPARE(visitor.result, (QStringList{
        "begin:document",
        "begin:keyval",
        "key:one",
        "value:two",
        "end:keyval",
        "end:document"
    }));
}
void VisitorTest::testSelectiveRecursion_data()
{
    QTest::addColumn<bool>("recurseDoc");
    QTest::addColumn<bool>("recurseKeyVal");
    QTest::addColumn<QStringList>("result");

    QTest::newRow("no recursion")     << false << false << QStringList{"doc"};
    QTest::newRow("recurse document") << true  << false << QStringList{"doc", "keyval"};
    QTest::newRow("recurse all")      << true  << true  << QStringList{"doc", "keyval", "key", "value"};
}

void VisitorTest::testSelectiveRecursion()
{
    QFETCH(bool, recurseDoc);
    QFETCH(bool, recurseKeyVal);
    QFETCH(QStringList, result);

    DocumentNode doc = createTestDoc();

    RecursingVisitor visitor;
    visitor.recurseIntoDoc = recurseDoc;
    visitor.recurseIntoKeyVal = recurseKeyVal;
    doc.accept(&visitor);

    QCOMPARE(visitor.result, result);
}

#include "visitortest.moc"
