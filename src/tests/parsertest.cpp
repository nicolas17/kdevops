/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <memory>

#include <QObject>
#include <QtTest>

#include "lexer.h"
#include "parser.h"
#include "ast.h"

class ParserTest : public QObject
{
    Q_OBJECT
private slots:

    void testKV();
    void testBool();
    void testEmptyLines();
    void testTable();
    void testInt();
};

QTEST_APPLESS_MAIN(ParserTest);

void ParserTest::testKV()
{
    //                                             11             1 11
    //                                 012345 6789 01 0123456 78901 23
    QString document = QStringLiteral("one = \"two\"\n three=\"four\"");
    Lexer lexer(document);
    Parser parser(&lexer);
    std::unique_ptr<DocumentNode> doc = parser.parse();
    QVERIFY(bool(doc));

    QCOMPARE(doc->keyvals.size(), 2ul);

    auto* keyval0 = doc->keyvals.at(0).get();
    QCOMPARE(keyval0->key.name, QStringLiteral("one"));
    QCOMPARE(keyval0->key.range, TextRange(0,0,0,3));

    auto* value0 = dynamic_cast<StringValueNode*>(keyval0->value.get());
    QVERIFY(value0);
    QCOMPARE(value0->strValue, QStringLiteral("two"));
    QCOMPARE(value0->range, TextRange(0,6,0,11));

    auto* keyval1 = doc->keyvals.at(1).get();
    QCOMPARE(keyval1->key.name, QStringLiteral("three"));
    QCOMPARE(keyval1->key.range, TextRange(1,1,1,6));

    auto* value1 = dynamic_cast<StringValueNode*>(keyval1->value.get());
    QVERIFY(value1);
    QCOMPARE(value1->strValue, QStringLiteral("four"));
    QCOMPARE(value1->range, TextRange(1,7,1,13));
}

/// Test key-value entry with a boolean value.
void ParserTest::testBool()
{
    //                                 0123456789
    QString document = QStringLiteral("key = true\n");
    Lexer lexer(document);
    Parser parser(&lexer);
    std::unique_ptr<DocumentNode> doc = parser.parse();
    QVERIFY(bool(doc));

    QCOMPARE(doc->keyvals.size(), 1ul);
    auto* keyval0 = doc->keyvals.at(0).get();
    QCOMPARE(keyval0->key.name, QStringLiteral("key"));
    QCOMPARE(keyval0->key.range, TextRange(0,0,0,3));

    auto* value0 = dynamic_cast<BoolValueNode*>(keyval0->value.get());
    QVERIFY(value0);
    QCOMPARE(value0->boolValue, true);
    QCOMPARE(value0->range, TextRange(0,6,0,10));
}


/// Test that empty lines don't break the parser.
void ParserTest::testEmptyLines()
{
    QString document = QStringLiteral("\n\none = \"two\"\n\n\n\na=\"b\"");
    Lexer lexer(document);
    Parser parser(&lexer);

    std::unique_ptr<DocumentNode> doc = parser.parse();
    QVERIFY(bool(doc));

    QCOMPARE(doc->keyvals.size(), 2ul);

    auto* keyval0 = doc->keyvals.at(0).get();
    QCOMPARE(keyval0->key.name, QStringLiteral("one"));
    QCOMPARE(keyval0->key.range, TextRange(2,0, 2,3));

    auto* value0 = dynamic_cast<StringValueNode*>(keyval0->value.get());
    QVERIFY(value0);
    QCOMPARE(value0->strValue, QStringLiteral("two"));
    QCOMPARE(value0->range, TextRange(2,6, 2,11));

    auto* keyval1 = doc->keyvals.at(1).get();
    QCOMPARE(keyval1->key.name, QStringLiteral("a"));
    QCOMPARE(keyval1->key.range, TextRange(6,0, 6,1));
}

/// Test simple parsing of a table.
void ParserTest::testTable()
{
    //                                 01234567 012345678901
    QString document = QStringLiteral("[group]\nkey1 = true");
    Lexer lexer(document);
    Parser parser(&lexer);

    std::unique_ptr<DocumentNode> doc = parser.parse();
    QVERIFY(bool(doc));

    QCOMPARE(doc->keyvals.size(), 0ul);
    QCOMPARE(doc->tables.size(), 1ul);

    auto* table0 = doc->tables.at(0).get();
    QVERIFY(table0);
    QCOMPARE(table0->name, QStringLiteral("group"));
    QCOMPARE(table0->nameRange, TextRange(0,1, 0,6));

    auto* keyval0 = table0->keyvals.at(0).get();
    QCOMPARE(keyval0->key.name, QStringLiteral("key1"));
    QCOMPARE(keyval0->key.range, TextRange(1,0, 1,4));

    // don't bother verifying the value
}
void ParserTest::testInt()
{
    //                                           1111111111
    //                                 01234567890123456789
    QString document = QStringLiteral("max-series = 123456");
    Lexer lexer(document);
    Parser parser(&lexer);

    std::unique_ptr<DocumentNode> doc = parser.parse();
    QVERIFY(bool(doc));

    QCOMPARE(doc->keyvals.size(), 1ul);
    auto* keyval0 = doc->keyvals.at(0).get();
    QCOMPARE(keyval0->key.name, QStringLiteral("max-series"));
    QCOMPARE(keyval0->key.range, TextRange(0,0,0,10));

    auto* value0 = dynamic_cast<IntValueNode*>(keyval0->value.get());
    QVERIFY(value0);
    QCOMPARE(value0->intValue, 123456);
    QCOMPARE(value0->range, TextRange(0,13,0,19));
}

#include "parsertest.moc"
