/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QtTest>

#include "lexer.h"

class LexerTest : public QObject
{
    Q_OBJECT
private slots:

    void testKV();
    void testRanges();
    void testBrackets();
    void testEOF();
    void testSkipComments();
    void testIncompleteString();
    void testBooleans();
    void testIntegers();
};

QTEST_APPLESS_MAIN(LexerTest);

void LexerTest::testKV()
{
    //                                              11
    //                                    012345678901
    QString document = QStringLiteral(R"d(one = "two")d");
    Lexer lexer(document);
    Token t;

    t = lexer.next(Lexer::MainLexerMode);
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("one"));
    QCOMPARE(t.range, TextRange(0,0,0,3));

    t = lexer.next(Lexer::MainLexerMode);
    QCOMPARE(t.type(), TokenType::EQUAL);
    QCOMPARE(t.range, TextRange(0,4,0,5));

    t = lexer.next(Lexer::ValueLexerMode);
    QCOMPARE(t.type(), TokenType::STRING);
    QCOMPARE(t.strValue(), QStringLiteral("two"));
    QCOMPARE(t.range, TextRange(0,6,0,11));

    t = lexer.next(Lexer::ValueLexerMode);
    QCOMPARE(t.type(), TokenType::END_OF_FILE);
}
void LexerTest::testRanges()
{
    //                                 012345678|012345
    QString document = QStringLiteral("one  two\nthree");
    Lexer lexer(document);
    Token t;

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("one"));
    QCOMPARE(t.range, TextRange(0,0,0,3));

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("two"));
    QCOMPARE(t.range, TextRange(0,5,0,8));

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::END_OF_LINE);

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("three"));
    QCOMPARE(t.range, TextRange(1,0,1,5));

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::END_OF_FILE);
}
void LexerTest::testBrackets()
{
    QString document = QStringLiteral("[one] [[two]] [  spaces  ]");
    Lexer lexer(document);
    Token t;

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::LBRACKET);

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("one"));

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::RBRACKET);


    t = lexer.next();
    QCOMPARE(t.type(), TokenType::DOUBLE_LBRACKET);

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("two"));

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::DOUBLE_RBRACKET);


    t = lexer.next();
    QCOMPARE(t.type(), TokenType::LBRACKET);

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("spaces"));

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::RBRACKET);

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::END_OF_FILE);
}

/// Test that #comments are skipped and don't produce a token, in either lexer mode.
void LexerTest::testSkipComments()
{
    QString document = QStringLiteral("one # ident 123 [table] \"string\"\ntwo = 3 # more comments\n");
    Lexer lexer(document);
    Token t;

    t = lexer.next(Lexer::MainLexerMode);
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("one"));

    t = lexer.next(Lexer::MainLexerMode);
    QCOMPARE(t.type(), TokenType::END_OF_LINE);

    t = lexer.next(Lexer::MainLexerMode);
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("two"));

    t = lexer.next(Lexer::MainLexerMode);
    QCOMPARE(t.type(), TokenType::EQUAL);

    t = lexer.next(Lexer::ValueLexerMode);
    QCOMPARE(t.type(), TokenType::INT);

    t = lexer.next(Lexer::ValueLexerMode);
    QCOMPARE(t.type(), TokenType::END_OF_LINE);

    t = lexer.next(Lexer::ValueLexerMode);
    QCOMPARE(t.type(), TokenType::END_OF_FILE);
}


/// Test that we can keep calling next() repeatedly after reaching EOF.
/// This simplifies the implementation of the parser.
void LexerTest::testEOF()
{
    QString document = QStringLiteral("lalala");
    Lexer lexer(document);
    Token t;

    t = lexer.next();
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("lalala"));

    for(size_t i = 0; i<100; ++i) {
        t = lexer.next();
        QCOMPARE(t.type(), TokenType::END_OF_FILE);
    }
}

void LexerTest::testIncompleteString()
{
    QString document = QStringLiteral("hello \"world");
    Lexer lexer(document);
    Token t;

    t = lexer.next(Lexer::MainLexerMode);
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("hello"));

    // I'm not sure if I care what the immediate tokens are here, but eventually we should
    // hit an EOF (and according to testEOF, once we hit an EOF, more next() calls will
    // keep returning EOF)
    t = lexer.next(Lexer::MainLexerMode);
    t = lexer.next(Lexer::MainLexerMode);
    t = lexer.next(Lexer::MainLexerMode);
    QCOMPARE(t.type(), TokenType::END_OF_FILE);
}

void LexerTest::testBooleans()
{
    //                                           111111
    //                                 0123456789012345
    QString document = QStringLiteral("true false TRUE");
    Lexer lexer(document);
    Token t;

    t = lexer.next(Lexer::ValueLexerMode);
    QCOMPARE(t.type(), TokenType::BOOL);
    QCOMPARE(t.intValue(), 1);
    QCOMPARE(t.range, TextRange(0,0,0,4));

    t = lexer.next(Lexer::ValueLexerMode);
    QCOMPARE(t.type(), TokenType::BOOL);
    QCOMPARE(t.intValue(), 0);
    QCOMPARE(t.range, TextRange(0,5,0,10));

    // ensure this is case-sensitive; TRUE is not a boolean
    // TODO: Back when the lexer only had one mode, this test ensured that TRUE was
    // tokenized as an identifier. Now, in normal mode both 'true' and 'TRUE' are
    // identifiers, and in value mode 'true' is a boolean, but it's not clear what 'TRUE'
    // should be in the value lexer. I may need an "invalid token" token, or read it as a
    // 'true' boolean with a warning, and that needs extra work.
    QEXPECT_FAIL("","Case-sensitive test disabled", Abort);

    t = lexer.next(Lexer::ValueLexerMode);
    QCOMPARE(t.type(), TokenType::IDENT);
    QCOMPARE(t.strValue(), QStringLiteral("TRUE"));
    QCOMPARE(t.range, TextRange(0,11,0,15));
}

void LexerTest::testIntegers() {
    // TODO check for 64-bit support
    QString document = QStringLiteral("1 123 -17 +92 1_000 5_349_221 1_2_3_4_5");
    Lexer lexer(document);
    Token t;

    for (int n : {1, 123, -17, 92, 1000, 5349221, 12345}) {
        t = lexer.next(Lexer::ValueLexerMode);
        QCOMPARE(t.type(), TokenType::INT);
        QCOMPARE(t.intValue(), n);
    }
}


#include "lexertest.moc"
