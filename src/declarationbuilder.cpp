/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "declarationbuilder.h"

#include <QDebug>
#include <language/duchain/types/integraltype.h>

#include "ast.h"

bool TOMLDeclarationBuilder::visit(KeyValNode* node)
{
    qDebug() << "visiting";
    KDevelop::IndexedString is(node->key.name);
    KDevelop::Identifier id(is);
    KDevelop::QualifiedIdentifier qi(id);

    const TextRange& myRange = node->key.range;
    KDevelop::RangeInRevision range(myRange.start.line, myRange.start.column, myRange.end.line, myRange.end.column);

    auto* decl = openDeclaration<KDevelop::Declaration>(qi, range);

    auto* valueNode = node->value.get();
    if (dynamic_cast<StringValueNode*>(valueNode)) {
        KDevelop::AbstractType::Ptr type(new KDevelop::IntegralType(KDevelop::IntegralType::TypeString));
        decl->setType(type);
    } else if (dynamic_cast<BoolValueNode*>(valueNode)) {
        KDevelop::AbstractType::Ptr type(new KDevelop::IntegralType(KDevelop::IntegralType::TypeBoolean));
        decl->setType(type);
    } else if (dynamic_cast<IntValueNode*>(valueNode)) {
        KDevelop::AbstractType::Ptr type(new KDevelop::IntegralType(KDevelop::IntegralType::TypeInt));
        decl->setType(type);
    } else {
        decl->setType(KDevelop::AbstractType::Ptr());
    }

    closeDeclaration();
    return false; // don't recurse; we already looked at the child nodes
}

bool TOMLDeclarationBuilder::visit(TableNode* table)
{
    const TextRange& myRange = table->nameRange;
    KDevelop::RangeInRevision range(myRange.start.line, myRange.start.column, myRange.end.line, myRange.end.column);
    KDevelop::QualifiedIdentifier name(table->name);

    auto* decl = openDeclaration<KDevelop::Declaration>(name, range);
    decl->setKind(KDevelop::Declaration::Instance);

    auto* context = openContext(table, KDevelop::DUContext::Class, name);
    for (auto& keyval: table->keyvals) {
        keyval->accept(this);
    }
    decl->setInternalContext(context);
    closeContext();
    closeDeclaration();
    return false;
}
