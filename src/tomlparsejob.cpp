/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tomlparsejob.h"

#include <QDebug>

#include <language/backgroundparser/urlparselock.h>
#include <language/duchain/problem.h>
#include <language/duchain/builders/abstractcontextbuilder.h>
#include <language/duchain/duchainutils.h>
#include <language/duchain/duchaindumper.h>

#include "ast.h"
#include "parser.h"
#include "lexer.h"
#include "declarationbuilder.h"

KDevTOMLParseJob::KDevTOMLParseJob(const KDevelop::IndexedString& url, KDevelop::ILanguageSupport* langSupport)
    : KDevelop::ParseJob(url, langSupport)
{
}

void KDevTOMLParseJob::run(ThreadWeaver::JobPointer self, ThreadWeaver::Thread* thread)
{
    KDevelop::UrlParseLock parseLock(document());
    qDebug() << "Parse job running";
    if (abortRequested()) {
        qDebug() << "Abort requested";
        return;
    }

    KDevelop::ProblemPointer p = readContents();
    if (p) {
        qDebug() << "Problem reading contents?";
        // TODO report the problem
        return;
    }

    KDevelop::ReferencedTopDUContext context;
    // mainly cargo-culted
    {
        KDevelop::DUChainReadLocker lock;
        context = KDevelop::DUChainUtils::standardContextForUrl(document().toUrl());
    }
    if (context) {
        translateDUChainToRevision(context);
    }

    Lexer lexer(QString::fromUtf8(contents().contents));
    Parser parser(&lexer);

    TOMLDeclarationBuilder builder;
    auto ast = parser.parse();
    if (ast) {
        KDevelop::DUChainWriteLocker lock(KDevelop::DUChain::lock());
        context = builder.build(document(), ast.get(), context);
    } else {
        qDebug() << "some parser error happened...";
    }

    qDebug() << "duchain built: " << context;

    if (abortRequested()) {
        return abortJob();
    }

    setDuChain(context);

    // more cargo-culting
    {
        KDevelop::DUChainWriteLocker lock;

        KDevelop::ParsingEnvironmentFilePointer file = context->parsingEnvironmentFile();
        Q_ASSERT(file);
        file->setModificationRevision(contents().modification);
        Q_ASSERT(context.data() == context->topContext());
        KDevelop::DUChain::self()->updateContextEnvironment(context->topContext(), file.data());
    }

    highlightDUChain();

    qDebug() << "emitting update-ready";
    KDevelop::DUChain::self()->emitUpdateReady(document(), duChain());
    qDebug() << "done!";

    KDevelop::DUChainReadLocker lock(KDevelop::DUChain::lock());
    KDevelop::DUChainDumper dumper;
    dumper.dump(context,5);
}
