/*
 * TOML parser
 * Copyright (C) 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "contextbuilder.h"

#include <QDebug>

#include "ast_visitor.h"
#include "ast.h"


KDevelop::TopDUContext* TOMLContextBuilder::newTopContext(const KDevelop::RangeInRevision& range, KDevelop::ParsingEnvironmentFile* file)
{
    static const KDevelop::IndexedString langString("TOML");
    Q_ASSERT(!file);
    file = new KDevelop::ParsingEnvironmentFile(document());
    file->setLanguage(langString);

    return new KDevelop::TopDUContext(document(), range, file);
}

void TOMLContextBuilder::startVisiting(AstNode* node)
{
    qDebug() << "startVisiting called";
    node->accept(this);
}
void TOMLContextBuilder::setContextOnNode(AstNode* node, KDevelop::DUContext* context)
{
    qDebug() << "dummy";
}
KDevelop::DUContext* TOMLContextBuilder::contextFromNode(AstNode* node)
{
    qDebug() << "dummy";
    return nullptr;
}
KDevelop::RangeInRevision TOMLContextBuilder::editorFindRange(AstNode* fromNode, AstNode* toNode)
{
    qDebug() << "dummy";
    return {};
}
KDevelop::QualifiedIdentifier TOMLContextBuilder::identifierForNode(AstNode* node)
{
    qDebug() << "dummy";
    return {};
}
