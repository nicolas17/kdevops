#=============================================================================
# Copyright 2017 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#=============================================================================

find_program(RE2C_EXECUTABLE NAMES re2c)

execute_process(COMMAND ${RE2C_EXECUTABLE} --version OUTPUT_VARIABLE RE2C_VERSION_OUTPUT)

string(REGEX REPLACE "^re2c ([0-9.]+).*" "\\1" RE2C_VERSION ${RE2C_VERSION_OUTPUT})

include(FindPackageHandleStandardArgs)
include(CMakeParseArguments)

find_package_handle_standard_args(
    Re2c
    REQUIRED_VARS RE2C_EXECUTABLE
    VERSION_VAR RE2C_VERSION
)

function(RE2C_TARGET name re2c_input re2c_output)
    cmake_parse_arguments(re2c "" "FLAGS" "" ${ARGN})
    separate_arguments(re2c_FLAGS)
    add_custom_command(
        OUTPUT ${re2c_output}
        COMMAND ${RE2C_EXECUTABLE} ${re2c_FLAGS} -o ${re2c_output} ${re2c_input}
        DEPENDS ${re2c_input}
        COMMENT "Building re2c scanner ${re2c_output}"
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    )
endfunction()
